package com.ellipticsoft.statemachine.eventsm.util;

import com.ellipticsoft.statemachine.eventsm.State;
import com.ellipticsoft.statemachine.eventsm.Stateable;

public class Request implements Stateable {

	private State state;

	@Override
	public State getState() {
		return state;
	}

	@Override
	public void setState(State state) {
		this.state = state;
	}

	public void hookState(State hookedState) {
		this.state = hookedState;
	}

}
