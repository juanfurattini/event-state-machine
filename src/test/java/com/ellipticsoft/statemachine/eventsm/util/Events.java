package com.ellipticsoft.statemachine.eventsm.util;

import com.ellipticsoft.statemachine.eventsm.Event;

public enum Events implements Event {

	GENERATE, PAUSE, CANCEL, FINALIZE;

	@Override
	public String getName() {
		return this.name();
	}
}
