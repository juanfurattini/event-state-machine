package com.ellipticsoft.statemachine.eventsm;

/**
 * 
 * @author juan.m.furattini
 *
 */
public interface Event {

	public String getName();

}
