package com.ellipticsoft.statemachine.eventsm;

/**
 * 
 * @author juan.m.furattini
 *
 */
public interface State {

	public String getName();

	public void beforeStateChange(State to, Object... args);

	public void afterStateChange(Object... args);

}
