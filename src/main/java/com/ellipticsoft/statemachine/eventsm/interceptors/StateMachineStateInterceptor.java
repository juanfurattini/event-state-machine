package com.ellipticsoft.statemachine.eventsm.interceptors;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import com.ellipticsoft.statemachine.eventsm.EventStateMachine;
import com.ellipticsoft.statemachine.eventsm.State;
import com.ellipticsoft.statemachine.eventsm.interceptors.utils.InterceptorUtils;

/**
 * 
 * @author juan.m.furattini
 *
 */
@Aspect
public class StateMachineStateInterceptor {

	/**
	 * 
	 * @param point
	 * @param nextState
	 * @return
	 * @throws Throwable
	 */
	@Around("execution(private * com.ellipticsoft.statemachine.eventsm.EventStateMachine.updateState(..)) && args(nextState)")
	public Object onUpdateState(ProceedingJoinPoint point, State nextState) throws Throwable {
		EventStateMachine esm = InterceptorUtils.getStateMachine(point);
		esm.getState().beforeStateChange(nextState);
		Object result = point.proceed();
		esm.getState().afterStateChange();
		return result;
	}

}
