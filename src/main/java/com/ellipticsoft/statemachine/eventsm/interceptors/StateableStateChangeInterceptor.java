package com.ellipticsoft.statemachine.eventsm.interceptors;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import com.ellipticsoft.statemachine.eventsm.EventStateMachine;
import com.ellipticsoft.statemachine.eventsm.Stateable;
import com.ellipticsoft.statemachine.eventsm.exceptions.IllegalStateModificationException;

/**
 * 
 * @author juan.m.furattini
 *
 */
@Aspect
public class StateableStateChangeInterceptor {

	/**
	 * 
	 * @param point
	 * @param stateable
	 * @throws IllegalStateModificationException
	 * @throws ClassNotFoundException
	 */
	@Before("execution(* setState(..)) && target(stateable)")
	public void onSetState(JoinPoint point, Stateable stateable)
			throws IllegalStateModificationException, ClassNotFoundException {
		Class<?> callerClass = Class.forName(Thread.currentThread().getStackTrace()[3].getClassName());
		if (!callerClass.equals(EventStateMachine.class)) {
			throw new IllegalStateModificationException("Only state machine is allowed to change state");
		}
	}

}
