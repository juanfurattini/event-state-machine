package com.ellipticsoft.statemachine.eventsm.interceptors;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import com.ellipticsoft.statemachine.eventsm.EventStateMachine;
import com.ellipticsoft.statemachine.eventsm.exceptions.ClosedStateMachineException;
import com.ellipticsoft.statemachine.eventsm.exceptions.StateCorruptedException;
import com.ellipticsoft.statemachine.eventsm.exceptions.StateMachineNotCompiledException;
import com.ellipticsoft.statemachine.eventsm.interceptors.utils.InterceptorUtils;

/**
 * 
 * @author juan.m.furattini
 *
 */
@Aspect
public class TransitionsInterceptor {

	/**
	 * 
	 * @param point
	 * @throws StateMachineNotCompiledException
	 * @throws StateCorruptedException
	 */
	@Before("execution(* com.ellipticsoft.statemachine.eventsm.EventStateMachine.transition(..))")
	public void onStateTransition(JoinPoint point) throws StateMachineNotCompiledException, StateCorruptedException {
		EventStateMachine stateMachine = InterceptorUtils.getStateMachine(point);
		if (!stateMachine.isCompiled()) {
			throw new StateMachineNotCompiledException("Can't change state with a non compiled state machine");
		}

		if (!stateMachine.getState().equals(stateMachine.getStateable().getState())) {
			throw new StateCorruptedException(
					"The stateable object state and the state machine state are different. Machine: "
							+ stateMachine.getState().getName() + " - Stateable: "
							+ stateMachine.getStateable().getState().getName());
		}
	}

	/**
	 * 
	 * @param point
	 * @throws Throwable
	 */
	@Before("execution(* com.ellipticsoft.statemachine.eventsm.EventStateMachine.addTransition(..))")
	public void onAddingTransitions(JoinPoint point) throws ClosedStateMachineException {
		EventStateMachine stateMachine = InterceptorUtils.getStateMachine(point);
		if (stateMachine.isCompiled()) {
			throw new ClosedStateMachineException("Can't add transitions to a compiled state machine");
		}
	}

}
