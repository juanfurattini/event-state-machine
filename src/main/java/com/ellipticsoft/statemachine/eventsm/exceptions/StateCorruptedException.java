package com.ellipticsoft.statemachine.eventsm.exceptions;

public class StateCorruptedException extends RuntimeException {

	private static final long serialVersionUID = -8933963328716691700L;

	public StateCorruptedException(String message) {
		super(message);
	}

}
