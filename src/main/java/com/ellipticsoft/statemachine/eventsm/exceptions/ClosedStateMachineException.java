package com.ellipticsoft.statemachine.eventsm.exceptions;

public class ClosedStateMachineException extends RuntimeException {

	private static final long serialVersionUID = 1991352794400087881L;

	public ClosedStateMachineException(String message) {
		super(message);
	}

}
