package com.ellipticsoft.statemachine.eventsm.exceptions;

public class IllegalStateModificationException extends RuntimeException {

	private static final long serialVersionUID = 488291120284276532L;

	public IllegalStateModificationException(String message) {
		super(message);
	}

}
