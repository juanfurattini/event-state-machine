package com.ellipticsoft.statemachine.eventsm;

/**
 * 
 * @author juan.m.furattini
 *
 */
public final class EventStateAssociation {

	private final Event event;
	private final State state;

	public EventStateAssociation(final Event event, final State state) {
		this.event = event;
		this.state = state;
	}

	public final Event getEvent() {
		return event;
	}

	public final State getState() {
		return state;
	}

}
